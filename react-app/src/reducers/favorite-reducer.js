import { createSlice } from "@reduxjs/toolkit"

export const favoriteSlice = createSlice({
  name: 'favorite',
  initialState: {
    countInFavorite: []
  },
  reducers: {
    addFavorite: (prevState, action) => {
      const click = action.payload
      const hasInFavorit = prevState.countInFavorite.some((prevClick) => prevClick.id === click.id)
      if (hasInFavorit) return prevState
      return {
        ...prevState,
        countInFavorite: [...prevState.countInFavorite, click]
      }
    },
    removeFavorite: (prevState, action) => {
      const click = action.payload
      return {
        ...prevState,
        countInFavorite: prevState.countInFavorite.filter((prevClick) => {
          return prevClick.id != click.id
        })
      }
    }
  }
})
export const { addFavorite, removeFavorite } = favoriteSlice.actions
export default favoriteSlice.reducer