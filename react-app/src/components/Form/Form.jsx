import React, { useEffect, useState } from "react"

import "./form.css"

let Form = () => {
  const [inputRating, setInputRating] = useState('')
  const [inputName, setInputName] = useState('')
  const [visitRating, setVisitRating] = useState(false)
  const [visitName, setVisitName] = useState(false)
  const [nameError, setNameError] = useState('Вы забыли указать имя и фамилию')
  const [ratingError, setRatingError] = useState('Укажите оценку')
  const [formValid, setFormValid] = useState(false)

  useEffect(() => {
    if (nameError || ratingError) {
      setFormValid(false)
    } else setFormValid(true)
  }, [nameError, ratingError])

  let classScore = "add-review__score-error add-review__score-error_visible"
  let className = "add-review__name-error add-review__name-error_visible"


  const nameHandler = (e) => {
    setInputName(e.target.value)
    if (e.target.value.length < 2) { setNameError('Имя не пожет быть короче 2-х символов') }
    else { setNameError('') }
  }
  const ratingHandler = (e) => {
    setInputRating(e.target.value)
    if (isNaN(e.target.value)) { setRatingError('Ошибка! Ввели не число') } else { setRatingError('') }
    if (e.target.value < 1 || e.target.value > 5) { setRatingError('Оценка должна быть от 1 до 5') } else { setRatingError('') }

  }
  const blurHandler = (e) => {
    switch (e.target.name) {
      case 'name':
        setVisitName(true)
        break
      case 'score':
        setVisitRating(true)
        break
    }
  }
  return (
    <form className="add-review" >
      <div className="first-line">
        <div><input value={inputName} className="add-review__name" type="text" name="name" onBlur={e => blurHandler(e)} placeholder="Имя и фамилия" onChange={(e) => nameHandler(e)} />
          {(visitName && nameError) && <div className={className}>{nameError}</div>}
        </div>
        <div><input className="add-review__score" value={inputRating} name="score" onBlur={e => blurHandler(e)} placeholder="Оценка"
          onChange={(e) => ratingHandler(e)} />
          {(visitRating && ratingError) && <div className={classScore}>{ratingError}</div>}
        </div>
      </div>
      <textarea className="add-review__second-line" name="text" id="#" cols="30" rows="10"
        placeholder="Текст отзыва"></textarea>
      <button disabled={!formValid} type="submit" className="add-review__button">Отправить отзыв</button>
    </form>
  )
}

export default Form
