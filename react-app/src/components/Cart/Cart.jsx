import React from "react";

import { useSelector } from "react-redux"
import { store } from "../../store"

const iphone = { id: 1234, name: 'Iphone 13' }
function Cart() {
  const products = useSelector((store) => store.cart.products)
  let count = products.length
  const hasInCart = products.some((prevProduct) => { return prevProduct.id === iphone.id })
  return (
    <div className="header__basket"><img src="/images/basket_header.svg" alt="Корзина" height="45" />
      {hasInCart ? (<div className="header__basket-count">{count}</div>) : null}
    </div>
  )
}
export default Cart

