import React from "react";

import { Link } from "react-router-dom"
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import "./pageIndex.css"

function PageIndex() {
  return <>
    <div className="first first_index">
      <Header />
      <div className="contener contener_index">
        <p className="indexParagraph">
          Здесь должно быть содержимое главной страницы.<br />
          Но в рамках курса главная страница  используется лишь<br />
          в демонстрационных целях<br />
          <Link to='/product'>Перейти на страницу товара</Link>
        </p>
      </div>
      <Footer />
    </div>
  </>
}
export default PageIndex



