import React, { useState } from "react";

import './buttonColor.css'

const buttons = [
  {
    image: '/images/color-1.png',
    label: 'красный'
  },
  {
    image: '/images/color-2.png',
    label: 'серый'
  },
  {
    image: '/images/color-3.png',
    label: 'розовый'
  },
  {
    image: '/images/color-4.png',
    label: 'синий'
  },
  {
    image: '/images/color-5.png',
    label: 'белый'
  },
  {
    image: '/images/color-6.png',
    label: 'черный'
  }
]

function ButtonColor(props) {
  const [color, setColor] = useState()

  return <>
    <div className="product-color__title">Цвет товара: {color}</div>
    <div className="product-color__color-selection">
      {buttons.map((button) => (
        <button onClick={() => { setColor(button.label); }} className="product-color__color-selection-button"
          style={{ border: `${button.label === color ? '2px solid #F36223' : ''}` }}
          key={button.label} > <img src={button.image}
            alt={button.label} /></button>))}
    </div>
  </>
}
export default ButtonColor