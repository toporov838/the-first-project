import React from "react";
import ButtonColor from "./ButtonColor/ButtonColor";
import "./colors.css"

function Colors() {
  return (
    <div className="product-color">
      <ButtonColor />
    </div>
  )
}
export default Colors