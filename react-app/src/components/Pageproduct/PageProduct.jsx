import React from "react"

import Colors from "./Colors/Colors"
import MemoryConf from "./MemoryConf/MemoryConf"
import ReviewList from "./RevewList/ReviewList"
import Form from "../Form/Form"
import Footer from "../Footer/Footer"
import Header from "../Header/Header"
import Breadcrumbs from "../Breadcrumds/Breadcrumbs"
import Sidebutton from "../Sidebar/Sidebutton/Sidebutton"
import SideHeart from "../Sidebar/SideHeart/SideHeart"


function PageProduct() {
  return (
    <div className="first">
      <Header />
      <div className="contener">
        <Breadcrumbs />
        <h2 className="name-smartfone">
          Смартфон Apple iPhone 13, синий
        </h2>
        <div className="foto-smartfone">
          <img src="/images/image-1.png" alt="image-1" />
          <img src="/images/image-2.png" alt="image-2" />
          <img src="/images/image-3.png" alt="image-3" />
          <img src="/images/image-4.png" alt="image-4" />
          <img src="/images/image-5.png" alt="image-5" />
        </div>
        <main>
          <div className="box-characteristics">
            <div className="all-characteristics">
              <Colors />
              <MemoryConf />
              <div className="product-features">
                <h3 className="product-features__title">Характеристики товара</h3>
                <ul className="product-features__list">
                  <li className="product-features__list-element">Экран: <b>6.1</b></li>
                  <li className="product-features__list-element">Встроенная память: <b>128 ГБ</b></li>
                  <li className="product-features__list-element">Операционная система: <b>iOS 15</b></li>
                  <li className="product-features__list-element">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
                  </li>
                  <li className="product-features__list-element">Процессор: <a
                    href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>A15
                      Bionic</b></a>
                  </li>
                  <li className="product-features__list-element">Вес: <b>173 г</b></li>
                </ul>
              </div>
              <div className="description">
                <h3 className="description__title">Описание</h3>
                <p className="description__element">Наша самая совершенная система двух камер.
                  <br />Особый взгляд на прочность дисплея.
                  <br />Чип, с которым всё супербыстро.
                  <br />Аккумулятор держится заметно дольше.
                  <br /><i>iPhone 13 - сильный мира всего.</i></p>

                <p className="description__element">Мы разработали совершенно новую схему расположения и развернули
                  объективы на 45 градусов. Благодаря
                  этому
                  внутри корпуса поместилась наилучшая система двух камер с увеличенной матрицей широкоугольной
                  камеры.
                  Кроме
                  того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И
                  повысили
                  скорость
                  работы матрицы на сверхширокоугольной камере.</p>
                <p className="description__element">Новая сверхширокоугольная камера видит больше деталей в тёмных
                  областях снимков. Новая
                  широкоугольная
                  камера
                  улавливает на 47% больше света для
                  более качественных фотографий и видео. Новая оптическая стабилизация со сдвигом матрицы обеспечит
                  чёткие
                  кадры
                  даже в неустойчивом положении.</p>
                <p className="description__element">Режим «Киноэффект» автоматически добавляет великолепные эффекты
                  перемещения фокуса и изменения
                  резкости.
                  Просто
                  начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая
                  красивый
                  эффект
                  размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого
                  человека
                  или
                  объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
              </div>
              <div className="comparison-of-models">
                <span className="comparison-of-models__title">Сравнение моделей</span>
                <table className="table">
                  <thead className="thead">
                    <tr>
                      <th>Модель</th>
                      <th>Вес</th>
                      <th>Высота</th>
                      <th>Ширина</th>
                      <th>Толщина</th>
                      <th>Чип</th>
                      <th>Оъбём памяти</th>
                      <th>Аккумулятор</th>
                    </tr>
                  </thead>
                  <tbody className="tbody">
                    <tr>
                      <td>iPhone 11</td>
                      <td>194 грамма</td>
                      <td>150.9 мм</td>
                      <td>75.5 мм</td>
                      <td>8.3 мм</td>
                      <td>A13 Bionic chip</td>
                      <td>до 128 Гб</td>
                      <td>До 17 часов</td>
                    </tr>
                    <tr>
                      <td>iPhone 12</td>
                      <td>164 грамма</td>
                      <td>146.7 мм</td>
                      <td>71.5 мм</td>
                      <td>7.4 мм</td>
                      <td>A14 Bionic chip</td>
                      <td>до 256 Гб</td>
                      <td>До 19 часов</td>
                    </tr>
                    <tr>
                      <td>iPhone 13</td>
                      <td>174 грамма</td>
                      <td>146.7 мм</td>
                      <td>71.5 мм</td>
                      <td>7.65 мм</td>
                      <td>A15 Bionic chip</td>
                      <td>до 512 Гб</td>
                      <td>До 19 часов</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div>
              <aside className="right-panel">
                <div className="price-block">
                  <div className="price-block__price-heart">
                    <div className="price-block__price">
                      <div className="price-block__old-price"><span className="price-block__old-price-level">79990₽</span>
                        <div className="price-block__discount">-8%</div>
                      </div><span className="price-block__new-price">67990₽</span>
                    </div>
                    <SideHeart />
                  </div>
                  <div className="price-block__delivery"><span className="price-block__text-delivery">Самовывоз в четверг, 1
                    сентября —
                    <b>бесплатно</b></span>
                    <span className="price-block__text-delivery">Курьером в четверг, 1 сентября —
                      <b>бесплатно</b></span>
                  </div>
                  <Sidebutton />
                </div>
                <div className="ad">Реклама<iframe className="ad__iframe" src="../../../ads.html">Браузер не поддерживает
                  фрейм</iframe>
                  <iframe className="ad__iframe" src="../../../ads.html">Браузер не поддерживает
                    фрейм</iframe>
                </div>
              </aside>
            </div>
          </div>
          <section className="section-reviews">
            <ReviewList />
          </section>
        </main>
        <div className="box-add-review">
          <p className="add-review__title">Добавить свой отзыв</p>
          <Form />
        </div>
      </div>
      <Footer />
    </div>
  )
}
export default PageProduct