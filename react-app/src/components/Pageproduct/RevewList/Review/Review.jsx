import React from "react"

import styles from "./review.module.css"

function Review(props) {
  const { user, separator } = props
  return (
    <>  <div className={styles.review}>
      <img src={user.image} className={styles.photo} alt="author1" />
      <div className={styles.content}>
        <div className={styles.name}>{user.name}</div>
        <img src={user.rating} className={styles.rating} alt="rating1" />
        <div className={styles.advantagesDisadvantages}>
          <div><strong>Опыт использования:</strong> {user.exp}</div>
          <div>
            <div><strong>Достоинства:</strong></div>{user.message1}
          </div>
          <div>
            <div><strong>Недостатки:</strong></div>{user.message2}
          </div>
        </div>
      </div>
    </div>
      {(separator < (separator !== -1)) ? <div className={styles.separator}></div> : null}
    </>)
}
export default Review
