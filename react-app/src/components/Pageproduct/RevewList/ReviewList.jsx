import React from "react"
import Review from "./Review/Review"
import "./reviewList.css"

const bodyReviews = [
  {
    image: "/images/author1.png",
    rating: "/images/rating1.png",
    name: "Марк Г.",
    exp: "менее месяца",
    message1: "это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.",
    message2: "к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное"
  },
  {
    image: "/images/author2.png",
    rating: "/images/rating2.png",
    name: "Валерий Коваленко.",
    exp: "менее месяца",
    message1: "OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго",
    message2: "Плохая ремонтопригодность"
  }

]
function ReviewList() {
  return <>
    <div className="section-reviews__up">
      <div className="section-reviews__name">
        <h3 className="section-reviews__title">Отзывы</h3>
        <span className="section-reviews__count">425</span>
      </div>
    </div>
    <div className="review__list">
      {bodyReviews.map((bodyR, index) => (<Review user={bodyR} separator={index} key={bodyR.image} />))}

    </div>

  </>
}

export default ReviewList