import React from "react"

import ButtonMemory from "./ButtonMemory/ButtonMemory"
import "./memoryConf.css"

function MemoryConf() {
  return (<>
    <div className="memory-configuration">
      <ButtonMemory />
    </div>
  </>)
}
export default MemoryConf



