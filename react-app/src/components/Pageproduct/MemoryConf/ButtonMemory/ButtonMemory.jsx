import React, { useState } from "react";

import "./buttonMemory.css"
const memory = ['128 гБ', '256 гБ', '512 гБ']

function ButtonMemory(props) {
  const [selectedMemory, setSelectedMemory] = useState()

  return (<>
    <span className="memory-configuration__title">Конфигурация памяти: {selectedMemory}</span>
    <div className="memory-configuration__buttons"></div>
    {memory.map((value) => (<button className='memory-configuration__button' style={{
      border: `${value === selectedMemory ? '2px solid #F36223' : ''}`
    }}
      onClick={() => {
        setSelectedMemory(value)
      }}
      key={value}>{value}</button>))
    }

  </>)
}
export default ButtonMemory