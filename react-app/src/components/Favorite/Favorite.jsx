import React from "react";

import "./favorite.css"
import { useSelector } from "react-redux"

function Favorite() {
  const favorite = useSelector((store) => store.favorite.countInFavorite)
  const count = favorite.length
  return <>
    <img className="" src="/images/heartInHeader.png" alt="Иконка избранное" height={43} />
    {count!==0 ? <div className="favorit-counter">{count}</div>: null}
  </>
}
export default Favorite