import React from "react"

import { addProduct, removeProduct } from "../../../reducers/cart-reducer"
import { useSelector, useDispatch } from "react-redux"
import { store } from "../../../store"

const iphone = { id: 1234, name: 'Iphone 13' }
function Sidebutton() {
  const products = useSelector((store) => store.cart.products)
  let count = products.length
  const hasInCart = products.some((prevProduct) => { return prevProduct.id === iphone.id })
  const dispatch = useDispatch()
  function handleAddProduct() {
    localStorage.setItem('iphone', 1234)
    const action = addProduct(iphone)
    dispatch(action)
  }
  function handleRemove() {
    localStorage.removeItem('iphone')
    const action = removeProduct(iphone)
    dispatch(action)
  }
  function checkLocSt(){
    if (localStorage.getItem('iphone')) handleAddProduct()
  }
  checkLocSt()
  return (
    <div>
      {!hasInCart ? (<button type="submit" className="price-block__button" onClick={handleAddProduct}>Добавить в корзину</button>)
        : (<button type="submit" className="price-block__button price-block__button_gray" onClick={handleRemove}>Товар уже в корзине</button>)}
    </div>
  )
}
export default Sidebutton