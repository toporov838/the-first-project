import React from "react";

import { useSelector, useDispatch } from "react-redux"
import { addFavorite, removeFavorite } from "../../../reducers/favorite-reducer";

const model = { id: 1234, name: 'iphone 13' }
function SideHeart() {
  const allFavorit = useSelector((store) => store.favorite.countInFavorite)
  const hasInFavorite = allFavorit.some((prevFavorit) => prevFavorit.id === model.id)
  const dispatch = useDispatch()

  function hanldeClick() { hasInFavorite ? dispatch(removeFavorite(model)):
    dispatch(addFavorite(model))
  }
  return <>
    <div className="plice-block__heart" onClick={hanldeClick}></div>
  </>
}
export default SideHeart