import React from "react";
import { Link } from "react-router-dom";
import Cart from "../Cart/Cart";
import Favorite from "../Favorite/Favorite";
import "./header.css"

function Header() {
  return <>
    <header className="header-up">
      <div className="header-all">
        <Link to="/" className="a_header"><div className="header">
          <div className="header__icon"><img src="/images/favicon.png" alt="Логотип" /></div>
          <div className="header__title"><span className="header__title header__title_color">Мой</span>Маркет</div>
        </div></Link>
        <div className="header-all__wpapper-icons">
          <Favorite />
          <Cart />
        </div>
      </div>
    </header>
  </>
}
export default Header