import React from "react"

import styled from "styled-components"

const Contener = styled.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  padding: '8px 0px',
  gap: '4px',
  flexWrap: 'wrap'
});
const list = ["Электроника", "Смартфоны и гаджеты", "Мобильные телефоны", "Apple"]


function Breadcrumbs() {

  return <>
    <nav>
      <Contener>
        {list.map((li, index) => {
          const isLast = index === (list.length - 1)
          return <>
            <a href="#" key = {li}>{li}</a>
            {!isLast && <span>{'>'}</span>}
          </>
        })}
      </Contener>
    </nav>
  </>
}

export default Breadcrumbs