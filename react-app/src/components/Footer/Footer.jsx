import React from "react";

import { useCurrentDate } from "@kundinos/react-hooks"

function LastDate() {
  const currentDate = useCurrentDate({ every: "day" });
  const fullYear = currentDate.getFullYear();
  return (
    <span>{fullYear}</span>
  );
}

function Footer() {
  return <>
    <footer className="footer">
      <div className="footer-all">
        <div className="footer-all__element-1"><b>© ООО «<span className="header__title_color">Мой</span>Маркет» 2018-<LastDate />.</b><br />Для
          уточнения информации звоните по номеру
          <a href="tel:+7 900 000 0000">{' '}+7 900 000 0000</a>,<br />а предложения по сотрудничеству отправляйте на почту
          <a href="mailto:partner@mymarket.com">{' '}partner@mymarket.com</a>
        </div>
        <div className="footer-all__element-2"><a href="#top">Наверх</a></div>
      </div>
    </footer>
  </>
}
export default Footer