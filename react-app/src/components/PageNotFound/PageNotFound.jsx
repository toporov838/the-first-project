import React from "react";

import Header from "../Header/Header";
import { Link } from "react-router-dom";

function PageNotFound(){
  return(
    <div className="first first_index">
      <Header />
      <div className="contener contener_index">
        <p className="indexParagraph">
          <span style={{fontSize: '70px'}}>404: Нет такой страницы</span><br />
          <Link to='/'>Перейти на главную страницу</Link>
        </p>
      </div>

    </div>
  )
}
export default PageNotFound