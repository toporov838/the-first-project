import React from 'react'

import { BrowserRouter, Routes, Route } from "react-router-dom"
import './App.css'
import PageIndex from './components/PageIndex/PageIndex';
import PageNotFound from './components/PageNotFound/PageNotFound';
import PageProduct from './components/Pageproduct/PageProduct';
import './components/Pageproduct/PageProduct.css'



function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<PageIndex />} />
        <Route path='/product' element={<PageProduct />} />
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
